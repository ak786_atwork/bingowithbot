package com.example.bingowithbot.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.example.bingowithbot.R;
import com.example.bingowithbot.helpers.Constants;

import java.util.HashMap;

public class AdapterToGenerateUserBingo extends BaseAdapter {

    private Context context;
    private HashMap<Integer, Integer> numbers;
    private BuildBingoOnClickInterface buildBingoOnClickInterface;



    public AdapterToGenerateUserBingo(Context context, BuildBingoOnClickInterface buildBingoOnClickInterface) {
        this.context = context;
        this.buildBingoOnClickInterface = buildBingoOnClickInterface;
    }

    @Override
    public int getCount() {
        return Constants.COLUMN_SIZE * Constants.ROW_SIZE;
    }

    @Override
    public Object getItem(int position) {
        return numbers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final TextView view =(TextView) LayoutInflater.from(context).inflate(R.layout.gridview_item,null);
        view.setText("");
        if (buildBingoOnClickInterface != null)
        {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buildBingoOnClickInterface.onTextViewClicked((TextView) v, position);
                }
            });
        }

        return view;
    }

    public interface BuildBingoOnClickInterface
    {
        void onTextViewClicked(TextView textView, int position);
    }
}
