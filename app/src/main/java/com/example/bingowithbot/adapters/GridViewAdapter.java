package com.example.bingowithbot.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.example.bingowithbot.R;
import com.example.bingowithbot.helpers.Bingo55;
import com.example.bingowithbot.helpers.Constants;

import java.util.HashMap;

public class GridViewAdapter extends BaseAdapter {

    private Context context;
    private HashMap<Integer, Integer> numbers;
    private OnClickInterface onClickInterface;
    private Bingo55 bingo;


    public GridViewAdapter(Context context, Bingo55 bingo, OnClickInterface onClickInterface) {
        this.context = context;
        this.bingo = bingo;
        this.numbers = bingo.getPositionHashMap();
        this.onClickInterface =onClickInterface;
    }

    @Override
    public int getCount() {
        return Constants.COLUMN_SIZE * Constants.ROW_SIZE;
    }

    @Override
    public Object getItem(int position) {
        return numbers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TextView view =(TextView) LayoutInflater.from(context).inflate(R.layout.gridview_item,null);
        view.setText(numbers.get(position).toString());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickInterface.onTextViewClicked(view, bingo);
            }
        });
        return view;
    }

    public interface OnClickInterface
    {
        void onTextViewClicked(TextView textView, Bingo55 bingo);
    }
}

