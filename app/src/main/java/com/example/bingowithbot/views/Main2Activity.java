package com.example.bingowithbot.views;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;


import com.example.bingowithbot.R;
import com.example.bingowithbot.adapters.GridViewAdapter;
import com.example.bingowithbot.helpers.Bingo55;
import com.example.bingowithbot.viewmodel.MainActivityViewModel;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


public class Main2Activity extends AppCompatActivity implements GridViewAdapter.OnClickInterface {

    private GridView userGridView, botGridView;
    private String PLAYER1 = "Anil";
    private String PLAYER2 = "BOT";
    private String turn = PLAYER1;
    int previousValue;
    private int chancePlayed = 0;

    private HashMap<String, Bingo55> players;
    Bingo55 userBingo1, userBingo2;
    MainActivityViewModel mainActivityViewModel;

    TextView showValueTextView;
    boolean inResponse = false;

    GridViewAdapter gridViewAdapter;

    private ArrayList<Bingo55> bingo55ArrayList;
    private boolean[] winningStatusOfPlayers;
    private int numberOfBots = 2;
    private int lastTurn;
    private int currentTurn;
    private Bingo55 playerHumanBingo;
    private boolean someoneWonTheGame;
    private Bingo55 tempBingo;
    private TextView showYourTurnTextview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        init();
    }


    private void init() {
        showValueTextView = findViewById(R.id.selected_values_textview);
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        mainActivityViewModel.getSelectedValueMutableData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {

                //my code
                for (int i = 0; i < numberOfBots + 1; i++) {
                    if (lastTurn == i)
                        continue;

                    //update other players bingo
                    tempBingo = bingo55ArrayList.get(i);
                    if (tempBingo.isNumberHasAlreadySelected(integer))
                        continue;
                    tempBingo.setValueAsCrossed(integer);
                    //color the textview if it is user
                    if (i == 0)
                    {
                        TextView textView = (TextView) userGridView.getChildAt(bingo55ArrayList.get(0).getValueHashMap().get(integer).getPosition());
                        textView.setAlpha(0.6f);
                        textView.setBackground(getResources().getDrawable(R.drawable.textview_after_clicked));
                        textView.setTextColor(getResources().getColor(R.color.white));
                    }

                    if (bingo55ArrayList.get(i).haveYouWon()) {
                        winningStatusOfPlayers[i] = true;
                        someoneWonTheGame = true;
                    }
                }

                if (someoneWonTheGame)
                {
                    showWinnerScreen(winningStatusOfPlayers);
//                    finish();
                    return;
                }

                if (currentTurn == 0)
                    showYourTurnTextview.setVisibility(View.VISIBLE);
                //allow the next player to play
                if (currentTurn != 0)
                {
                    tempBingo = bingo55ArrayList.get(currentTurn);
                    performNextMoveForBot(tempBingo);
                }


            }
        });

        playerHumanBingo = new Bingo55();

        Intent intent = getIntent();
        if (intent.getSerializableExtra("userBingo") != null) {
            playerHumanBingo.setPositionHashMap((HashMap<Integer, Integer>) intent.getSerializableExtra("userBingo"));
        }

        userGridView = findViewById(R.id.user_gridview);

        botGridView.setAdapter(new GridViewAdapter(getBaseContext(), userBingo2, this));



        //changed generic for bots
        showYourTurnTextview = findViewById(R.id.your_turn_textview);
        bingo55ArrayList = new ArrayList<>();
        winningStatusOfPlayers = new boolean[numberOfBots + 1];
        currentTurn = 0;
        lastTurn = 0;
        bingo55ArrayList.add(playerHumanBingo);
        gridViewAdapter = (new GridViewAdapter(getBaseContext(), playerHumanBingo, this));
        userGridView.setAdapter(gridViewAdapter);
        for (int i = 1; i <= numberOfBots; i++) {
            bingo55ArrayList.add(new Bingo55());
        }

        printAllBotsSheet();

    }

    public void printAllBotsSheet()
    {
        for (int i = 1; i <= numberOfBots; i++)
        {
            Log.d("SHEETS", bingo55ArrayList.get(i).getFormattedBotSheet());

        }
    }

    private void performNextMoveForBot(Bingo55 bingo) {
        //call bingo next move method , get position , find textview , perform click
        int position = bingo.getNextMovePosition();
        if (position > -1) {
            int value = bingo.getPositionHashMap().get(position);
           // post value in live data and update text view
            showValueTextView.setText(String.valueOf(value));
            //update bingo
            bingo.setValueAsCrossed(value);

            if (bingo.haveYouWon()) {
                winningStatusOfPlayers[currentTurn] = true;
                someoneWonTheGame = true;
            }
            //update turn
            lastTurn = currentTurn;
            currentTurn = ++currentTurn % (numberOfBots + 1);

            mainActivityViewModel.getSelectedValueMutableData().postValue(value);
        }
    }

    private void showWinnerScreen(boolean[] winningStatusOfPlayers) {
        Intent intent = new Intent(this, ShowWinnerActivity.class);
        intent.putExtra("winners",winningStatusOfPlayers);
        startActivity(intent);
    }


    @Override
    public void onTextViewClicked(TextView textView, Bingo55 bingo) {

        //my code
        // player turn
        if (currentTurn == 0) {
            //update bingo , update ui, toggle turn , post value
            if (textView.getText() == null || textView.getText() == "")
                return;
            textView.setAlpha(0.6f);
            textView.setBackground(getResources().getDrawable(R.drawable.textview_after_clicked));
            textView.setTextColor(getResources().getColor(R.color.white));
            int value = Integer.parseInt(textView.getText().toString());

            if (bingo.isNumberHasAlreadySelected(value))
                return;
            bingo.setValueAsCrossed(value);

            if (bingo55ArrayList.get(0).haveYouWon()) {
                winningStatusOfPlayers[0] = true;
                someoneWonTheGame = true;
            }
            //update turn
            lastTurn = currentTurn;
            currentTurn = ++currentTurn % (numberOfBots + 1);

            //post value
            showValueTextView.setText(String.valueOf(value));
            showYourTurnTextview.setVisibility(View.INVISIBLE);
            mainActivityViewModel.getSelectedValueMutableData().postValue(value);


        }




     /*
        chancePlayed++;
        String userType = bingo.getUserType();
        if (turn.equals(PLAYER1) && bingo == userBingo1 || turn.equals(PLAYER2) && bingo == userBingo2) {
            //update bingo , update ui, toggle turn , post value
            if (textView.getText() == null || textView.getText() == "")
                return;

            showToast("clicked" + textView.getText().toString());
            //changing background

            textView.setAlpha(0.6f);
            textView.setBackground(getResources().getDrawable(R.drawable.textview_after_clicked));
            textView.setTextColor(getResources().getColor(R.color.white));
            int value = Integer.parseInt(textView.getText().toString());

            if (bingo.isNumberHasAlreadySelected(value))
                return;
            bingo.setValueAsCrossed(value);

            //bot
            if (bingo.getUserType() == PLAYER2 && bingo.isCheckLines())  // && chancePlayed > 12
                bingo.setCheckLines(true);

            if (bingo.haveYouWon()) {
                Intent intent = new Intent(this, ShowWinnerActivity.class);
                intent.putExtra("winner", bingo.getUserType());
                startActivity(intent);
                finish();
                return;
            }

            //user mistake
            if (turn.equals(PLAYER1) && inResponse && value != mainActivityViewModel.getSelectedValueMutableData().getValue()) {
                //  if (value != mainActivityViewModel.getSelectedValueMutableData().getValue())
                inResponse = false;

            }

            if (!inResponse) {
                toggleTurn();
                mainActivityViewModel.getSelectedValueMutableData().postValue(value);
                if (mainActivityViewModel.getSelectedValueMutableData().getValue() != null) {
                    previousValue = mainActivityViewModel.getSelectedValueMutableData().getValue();
                }
                showValueTextView.setText(String.valueOf(value));
            }
            //bot next move
            if (turn.equals(PLAYER2) && inResponse) {
                inResponse = false;
                performNextMoveForBot();

            }
            if (turn.equals(PLAYER1) && inResponse) {
                inResponse = false;

            }
        }*/
    }

    private void toggleTurn() {
        turn = turn.equals(PLAYER1) ? PLAYER2 : PLAYER1;

    }

    private void performNextMoveForBot() {
        //call bingo next move method , get position , find textview , perform click
        int position = userBingo2.getNextMovePosition();
        if (position > -1) {
            botGridView.getChildAt(position).performClick();
        }
    }

    private void showToast(String s) {
        //Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void btnClicked(View view) {

        //both working
//        userGridView.getChildAt(1).performClick();
//        gridViewAdapter.getView(1,null,null).performClick();

    }
}
