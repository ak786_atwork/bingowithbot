package com.example.bingowithbot.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;


import com.example.bingowithbot.R;
import com.example.bingowithbot.adapters.GridViewAdapter;
import com.example.bingowithbot.helpers.Bingo55;
import com.example.bingowithbot.viewmodel.MainActivityViewModel;

import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


public class MainActivity extends AppCompatActivity implements GridViewAdapter.OnClickInterface {

    private GridView userGridView, botGridView;
    private String PLAYER1 = "Anil";
    private String PLAYER2 = "BOT";
    private String turn = PLAYER1;
    int previousValue;
    private int chancePlayed = 0;

    private HashMap<String, Bingo55> players;
    Bingo55 userBingo1, userBingo2;
    MainActivityViewModel mainActivityViewModel;

    TextView showValueTextView;
    boolean inResponse = false;

    GridViewAdapter gridViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       init();
    }


    private  void init()
    {
        showValueTextView = findViewById(R.id.selected_values_textview);
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        mainActivityViewModel.getSelectedValueMutableData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {

                //update ui and bingo
                int value = integer;
                inResponse = true;
                if (turn.equals(PLAYER1)) {
                    //user  check whether number exists or not , if exists , set inresponse true
                    if (userBingo1.getValueHashMap().get(integer) == null) {
                        inResponse = false;
                    } else if (userBingo1.isNumberHasAlreadySelected(integer)) {
                        inResponse = false;
                    }

                } else {
                    //bot , also perform next move
                    if (userBingo2.getValueHashMap().get(integer) == null) {
                        inResponse = false;
                        performNextMoveForBot();

                    } else {
//                        userBingo2.setValueAsCrossed(integer);
                        if (userBingo2.isNumberHasAlreadySelected(integer)) {
                            inResponse = false;
                            performNextMoveForBot();
                        } else {
                            botGridView.getChildAt(userBingo2.getValueHashMap().get(integer).getPosition()).performClick();
                        }

                    }

                }

            }
        });
        userBingo1 = new Bingo55();
        userBingo2 = new Bingo55();

        Intent intent = getIntent();
        if (intent.getSerializableExtra("userBingo") != null)
        {
            userBingo1.setPositionHashMap((HashMap<Integer, Integer>) intent.getSerializableExtra("userBingo"));
        }
        //setting the names
        userBingo1.setUserType(PLAYER1);
        userBingo2.setUserType(PLAYER2);

        players = new HashMap<>();
        players.put(PLAYER1, userBingo1);
        players.put(PLAYER2, userBingo2);

        userGridView = findViewById(R.id.user_gridview);
        botGridView = findViewById(R.id.bot_gridview);

        findViewById(R.id.hide_bot_textview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleBotGridViewVisibility();
            }
        });

//        gridView.setColumnWidth(9);
        gridViewAdapter = (new GridViewAdapter(getBaseContext(), userBingo1, this));
        userGridView.setAdapter(gridViewAdapter);
        botGridView.setAdapter(new GridViewAdapter(getBaseContext(), userBingo2, this));


    }

    private void toggleBotGridViewVisibility() {
        int state = botGridView.getVisibility() == View.INVISIBLE ? View.VISIBLE : View.INVISIBLE;
        botGridView.setVisibility(state);
    }

    @Override
    public void onTextViewClicked(TextView textView, Bingo55 bingo) {

        chancePlayed++;
        String userType = bingo.getUserType();
        if (turn.equals(PLAYER1) && bingo == userBingo1 || turn.equals(PLAYER2) && bingo == userBingo2) {
            //update bingo , update ui, toggle turn , post value
            if (textView.getText() == null || textView.getText() == "")
                return;

            showToast("clicked" + textView.getText().toString());
            //changing background

            textView.setAlpha(0.6f);
            textView.setBackground(getResources().getDrawable(R.drawable.textview_after_clicked));
            textView.setTextColor(getResources().getColor(R.color.white));
            int value = Integer.parseInt(textView.getText().toString());

            if (bingo.isNumberHasAlreadySelected(value))
                return;
            bingo.setValueAsCrossed(value);

            //bot
            if(bingo.getUserType() == PLAYER2 && bingo.isCheckLines())  // && chancePlayed > 12
                bingo.setCheckLines(true);

            if (bingo.haveYouWon())
            {
                Intent intent = new Intent(this, ShowWinnerActivity.class);
                intent.putExtra("winner" , bingo.getUserType());
                startActivity(intent);
                finish();
                return;
            }

            //user mistake
            if (turn.equals(PLAYER1) && inResponse && value != mainActivityViewModel.getSelectedValueMutableData().getValue()) {
              //  if (value != mainActivityViewModel.getSelectedValueMutableData().getValue())
                    inResponse = false;

            }

            if (!inResponse) {
                toggleTurn();
                mainActivityViewModel.getSelectedValueMutableData().postValue(value);
                if (mainActivityViewModel.getSelectedValueMutableData().getValue() != null) {
                    previousValue = mainActivityViewModel.getSelectedValueMutableData().getValue();
                }
                showValueTextView.setText(String.valueOf(value));
            }
            //bot next move
            if (turn.equals(PLAYER2) && inResponse) {
                inResponse = false;
                performNextMoveForBot();

            }
            if (turn.equals(PLAYER1) && inResponse) {
                inResponse = false;

            }
        }
    }

    private void toggleTurn() {
        turn = turn.equals(PLAYER1) ? PLAYER2 : PLAYER1;

    }

    private void performNextMoveForBot() {
        //call bingo next move method , get position , find textview , perform click
        int position = userBingo2.getNextMovePosition();
        if (position > -1) {
            botGridView.getChildAt(position).performClick();
        }
    }

    private void showToast(String s) {
        //Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void btnClicked(View view) {

        //both working
//        userGridView.getChildAt(1).performClick();
//        gridViewAdapter.getView(1,null,null).performClick();

    }
}
