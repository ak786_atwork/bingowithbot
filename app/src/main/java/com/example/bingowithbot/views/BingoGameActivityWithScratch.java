package com.example.bingowithbot.views;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;


import com.example.bingowithbot.R;
import com.example.bingowithbot.adapters.GridViewAdapter;
import com.example.bingowithbot.adapters.GridViewAdapterScratch;
import com.example.bingowithbot.helpers.Bingo55;
import com.example.bingowithbot.helpers.CustomFontTextView;
import com.example.bingowithbot.helpers.Util;
import com.example.bingowithbot.viewmodel.MainActivityViewModel;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


public class BingoGameActivityWithScratch extends AppCompatActivity implements GridViewAdapterScratch.OnClickInterface {

    private GridView userGridView;
    private String PLAYER1 = "Anil";
    private String PLAYER2 = "BOT";
    private String turn = PLAYER1;
    int previousValue;
    private int chancePlayed = 0;

    private HashMap<String, Bingo55> players;
    Bingo55 userBingo1, userBingo2;
    MainActivityViewModel mainActivityViewModel;

    TextView showValueTextView;
    boolean inResponse = false;

    GridViewAdapterScratch gridViewAdapter;

    private ArrayList<Bingo55> bingo55ArrayList;
    private boolean[] winningStatusOfPlayers;
    private int numberOfBots = 2;
    private int lastTurn;
    private int currentTurn;
    private Bingo55 playerHumanBingo;
    private boolean someoneWonTheGame;
    private Bingo55 tempBingo;
    private TextView showYourTurnTextview;
    private Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        init();
    }


    private void init() {
        showValueTextView = findViewById(R.id.selected_values_textview);
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        mainActivityViewModel.getSelectedValueMutableData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {

                //my code
                for (int i = 0; i < numberOfBots + 1; i++) {
                    if (lastTurn == i)
                        continue;

                    //update other players bingo
                    tempBingo = bingo55ArrayList.get(i);
                    if (tempBingo.isNumberHasAlreadySelected(integer))
                        continue;
                    tempBingo.setValueAsCrossed(integer);

                    //color the textview if it is user
                    if (i == 0) {

                        CustomFontTextView textView = (CustomFontTextView) userGridView.getChildAt(bingo55ArrayList.get(0).getValueHashMap().get(integer).getPosition());
                        changeTextViewBackground(textView);
                        checkForScratchLines();

                    }

                    if (bingo55ArrayList.get(i).haveYouWon()) {
                        winningStatusOfPlayers[i] = true;
                        someoneWonTheGame = true;
                    }
                }

                if (someoneWonTheGame) {
                    showWinnerScreen(winningStatusOfPlayers);
//                    finish();
                    return;
                }

                if (currentTurn == 0)
                    showYourTurnTextview.setVisibility(View.VISIBLE);
                //allow the next player to play
                if (currentTurn != 0) {
                    tempBingo = bingo55ArrayList.get(currentTurn);
                    performNextMoveForBot(tempBingo);
                }


            }
        });

        playerHumanBingo = new Bingo55();

        Intent intent = getIntent();
        if (((HashMap<Integer, Integer>)intent.getSerializableExtra("userBingo")).size() > 0) {
            playerHumanBingo.setPositionHashMap((HashMap<Integer, Integer>) intent.getSerializableExtra("userBingo"));
        }



        //changed generic for bots
        handler = new Handler();

        showYourTurnTextview = findViewById(R.id.your_turn_textview);
        bingo55ArrayList = new ArrayList<>();
        winningStatusOfPlayers = new boolean[numberOfBots + 1];
        currentTurn = 0;
        lastTurn = 0;
        bingo55ArrayList.add(playerHumanBingo);
        gridViewAdapter = (new GridViewAdapterScratch(getBaseContext(), playerHumanBingo, this));
        userGridView = findViewById(R.id.user_gridview);
        userGridView.setAdapter(gridViewAdapter);
        for (int i = 1; i <= numberOfBots; i++) {
            bingo55ArrayList.add(new Bingo55());
        }

        printAllBotsSheet();

    }

    public void printAllBotsSheet() {
        for (int i = 1; i <= numberOfBots; i++) {
            Log.d("SHEETS", bingo55ArrayList.get(i).getFormattedBotSheet());

        }
    }


    private void performNextMoveForBot(final Bingo55 bingo) {
        //call bingo next move method , get position , find textview , perform click
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int position = bingo.getNextMovePosition();
                if (position > -1) {
                    int value = bingo.getPositionHashMap().get(position);
                    // post value in live data and update text view
                    showValueTextView.setText(String.valueOf(value));
                    //update bingo
                    bingo.setValueAsCrossed(value);

                    if (bingo.haveYouWon()) {
                        winningStatusOfPlayers[currentTurn] = true;
                        someoneWonTheGame = true;
                    }
                    //update turn
                    lastTurn = currentTurn;
                    currentTurn = ++currentTurn % (numberOfBots + 1);

                    mainActivityViewModel.getSelectedValueMutableData().postValue(value);
                }
            }
        },1000);

    }

    private void showWinnerScreen(boolean[] winningStatusOfPlayers) {
        Intent intent = new Intent(this, ShowWinnerActivity.class);
        intent.putExtra("winners", winningStatusOfPlayers);
        startActivity(intent);
    }


    @Override
    public void onTextViewClicked(CustomFontTextView textView) {

        //my code
        // player turn
        if (currentTurn == 0) {
            Bingo55 bingo = bingo55ArrayList.get(0);
            //update bingo , update ui, toggle turn , post value
            if (textView.getText() == null || textView.getText() == "")
                return;

            int value = Integer.parseInt(textView.getText().toString());

            if (bingo.isNumberHasAlreadySelected(value))
                return;
            bingo.setValueAsCrossed(value);
            changeTextViewBackground(textView);
            checkForScratchLines();


            if (bingo.haveYouWon()) {
                winningStatusOfPlayers[0] = true;
                someoneWonTheGame = true;
            }
            //update turn
            lastTurn = currentTurn;
            currentTurn = ++currentTurn % (numberOfBots + 1);

            //post value
            showValueTextView.setText(String.valueOf(value));
            showYourTurnTextview.setVisibility(View.INVISIBLE);
            mainActivityViewModel.getSelectedValueMutableData().postValue(value);

        }
    }


    private void changeTextViewBackground(CustomFontTextView textView) {
        textView.setAlpha(0.6f);
        textView.setBackground(getResources().getDrawable(R.drawable.textview_after_clicked));
        textView.setTextColor(getResources().getColor(R.color.white));
    }

    public void checkForScratchLines() {
        int linesToScratch[] = playerHumanBingo.getLinesToScratch();
        for (int i = 0; i < 4; i++) {
            if (linesToScratch[i] != -1) {
                switch (i) {
                    case 0:
                        // row
                        scratchRow(linesToScratch[i]);
                        break;

                    case 1:
                        // column
                        scratchColumn(linesToScratch[i]);
                        break;

                    case 2:
                        // first diagonal
                        scratchDiagonal1();
                        break;

                    case 3:
                        // second diagonal
                        scratchDiagonal2();
                        break;
                }
            }
        }

    }

/*    public void scratchBingoTextViews(int number) {

        for (int i = 0; i < number; i++) {
            TextView view = findViewById(bingoTextviewsIdArray[i]);
            //scale the view
            Util.scaleView(view);
            //change the background
            view.setBackground(getResources().getDrawable(R.drawable.first_diagonal));
            view.setTextColor(getResources().getColor(R.color.black));
        }
    }*/

    public void scratchRow(int rowNumber) {
        //get all gridview item in a row and change background
        rowNumber *= 5;
        CustomFontTextView view;
        for (int i = rowNumber; i < rowNumber + 5; i++) {
            view = (CustomFontTextView) userGridView.getChildAt(i);
            //scale the view
            Util.scaleView(view);
            //change the background
//            view.setBackground(getResources().getDrawable(R.drawable.horizontal));
//            view.setTextColor(getResources().getColor(R.color.black));
            view.setHorizontalLine(true);
            view.reDraw();
        }
    }

    public void scratchColumn(int columnNumber) {
        int i = 0;
        CustomFontTextView view;
        while (i < 5) {
            view = (CustomFontTextView) userGridView.getChildAt(columnNumber);
            //scale the view
            Util.scaleView(view);
            //change the background
//            view.setBackground(getResources().getDrawable(R.drawable.vertical));
//            view.setTextColor(getResources().getColor(R.color.black));
            view.setVerticalLine(true);
            view.reDraw();
            columnNumber += 5;
            i++;
        }
    }

    public void scratchDiagonal1() {
        int i = 0;
        CustomFontTextView view;
        int position = 0;
        while (i < 5) {
            view = (CustomFontTextView) userGridView.getChildAt(position);
            //scale the view
            Util.scaleView(view);
            //change the background
//            view.setBackground(getResources().getDrawable(R.drawable.first_diagonal));
//            view.setTextColor(getResources().getColor(R.color.black));
            view.setDiagonal1(true);
            view.reDraw();
            position += 6;
            i++;
        }
    }

    public void scratchDiagonal2() {
        int i = 0;
        CustomFontTextView view;
        int position = 4;
        while (i < 5) {
            view = (CustomFontTextView) userGridView.getChildAt(position);
            //scale the view
            Util.scaleView(view);
            //change the background
//            view.setBackground(getResources().getDrawable(R.drawable.second_diagonal));
//            view.setTextColor(getResources().getColor(R.color.black));
            view.setDiagonal2(true);
            view.reDraw();
            position += 4;
            i++;
        }
    }


}
