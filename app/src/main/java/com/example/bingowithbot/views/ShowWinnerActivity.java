package com.example.bingowithbot.views;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.bingowithbot.R;

import androidx.appcompat.app.AppCompatActivity;


public class ShowWinnerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_winner);
        Intent intent = getIntent();

      /*  if (intent != null)
        {
            String winner =  intent.getStringExtra("winner");

            TextView textView = findViewById(R.id.show_winner);
            ImageView imageView = findViewById(R.id.imageview);
            if (winner.equals("BOT"))
            {
                imageView.setImageResource(R.drawable.loser);
                textView.setText( " BOT se har gya ye...\n hahahahah");
            }
            else
            {
                imageView.setImageResource(R.drawable.winner);
                textView.setText(winner + " won the game");
            }

        }*/


        if (intent != null)
        {
            boolean[] winners =  intent.getBooleanArrayExtra("winners");
            StringBuilder winnersName = new StringBuilder();
            for (int i=0;i<winners.length;i++)
            {
                if (winners[i])
                {
                    if (i == 0)
                        winnersName.append("you won the game\n");
                    else
                        winnersName.append("bot"+i+" won the game \n");
                }
            }
            TextView textView = findViewById(R.id.show_winner);
            Log.d("WINNERS",winnersName.toString());
            textView.setText(winnersName.toString());
            ImageView imageView = findViewById(R.id.imageview);
            if (!winners[0])
            {
                imageView.setImageResource(R.drawable.loser);
            }
            else
            {
                imageView.setImageResource(R.drawable.winner);
            }

        }
    }
}
