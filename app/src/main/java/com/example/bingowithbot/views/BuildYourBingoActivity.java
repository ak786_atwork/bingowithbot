package com.example.bingowithbot.views;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;


import com.example.bingowithbot.R;
import com.example.bingowithbot.adapters.AdapterToGenerateUserBingo;
import com.example.bingowithbot.helpers.Bingo55;
import com.example.bingowithbot.helpers.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;

public class BuildYourBingoActivity extends AppCompatActivity implements AdapterToGenerateUserBingo.BuildBingoOnClickInterface, View.OnClickListener {

    private int numberCounter = 1;
    private int[][] bingoMatrix;
    //position value hashmap
    private HashMap<Integer, Integer> userHashMap;
    private HashMap<Integer, Integer> randomNumberHashMap;

    private AdapterToGenerateUserBingo gridViewAdapter;
    private AdapterToGenerateUserBingo adapterForRandomNumberGridView;
    private GridView userGridView;
    private GridView randomNumberGridView;
    private TextView playTextView;
    private TextView shuffleTextView;
    private boolean shuffledClicked;
    private ArrayList<Integer> positionArrayList;
    private ArrayList<Integer> valueArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_build_your_bingo);


        bingoMatrix = new int[Constants.ROW_SIZE][Constants.COLUMN_SIZE];
        gridViewAdapter = new AdapterToGenerateUserBingo(this, this);
        userGridView = findViewById(R.id.user_gridview);
        randomNumberGridView = findViewById(R.id.random_number_gridview);

        playTextView = findViewById(R.id.play_textview);
        shuffleTextView = findViewById(R.id.shuffle_textview);
        playTextView.setOnClickListener(this);
        shuffleTextView.setOnClickListener(this);


        positionArrayList = new ArrayList<>();
        valueArrayList = new ArrayList<>();
        randomNumberHashMap = new HashMap<>();
        initAndShuffleArraylists();
//        initRandomHashMap();
        adapterForRandomNumberGridView = new AdapterToGenerateUserBingo(this, null);
        randomNumberGridView.setAdapter(adapterForRandomNumberGridView);

        userHashMap = new HashMap<>();
        userGridView.setAdapter(gridViewAdapter);

    }

    private void initAndShuffleArraylists() {
        positionArrayList.clear();
        valueArrayList.clear();
        for (int i = 1; i < 26; i++) {
            positionArrayList.add(i - 1);
            valueArrayList.add(i);
        }
        Collections.shuffle(positionArrayList);
        Collections.shuffle(valueArrayList);
    }

    private void initRandomHashMap() {
        randomNumberHashMap = (new Bingo55()).prepareRandomBingoSheet();
    }

    @Override
    public void onTextViewClicked(TextView textView, int position) {
        if (userHashMap.get(position) == null) {
            textView.setText(String.valueOf(numberCounter));
            userHashMap.put(position, numberCounter);

            fillRandomGridView(position, numberCounter);
            numberCounter++;
        }
        if (numberCounter > 25) {
            playTextView.setVisibility(View.VISIBLE);
        }
    }

    private void fillRandomGridView(int position, int numberCounter) {
        /*if (shuffledClicked) {
            // regenerate all sheet
            fillAllEntriesInRandomGridView();
            shuffledClicked = false;
        } else {
            //just add this number on grid view and remove this number from random hashmap
            randomNumberHashMap.put(positionArrayList.remove(position), valueArrayList.remove(numberCounter));
            fillRestEntriesInRandomGridView();
        }*/
        fillAllEntriesInRandomGridView();
    }

    private void fillRestEntriesInRandomGridView() {
        //rest is randomly filled
        Integer position ;
        Integer value ;
        while (positionArrayList.size() > 0)
        {
            //prepare the random hashmap also and remove position and value from respective arraylist
            position = positionArrayList.remove(0);
            value =  valueArrayList.remove(0);
            randomNumberHashMap.put(position,value);

            ((TextView) randomNumberGridView.getChildAt(position)).setText(String.valueOf(value));
        }
        /*for (int i = 0; i < positionArrayList.size(); i++) {
            //prepare the random hashmap also and remove position and value from respective arraylist
            position = positionArrayList.remove(i);
            value =  valueArrayList.remove(i);
            randomNumberHashMap.put(position,value);

            ((TextView) randomNumberGridView.getChildAt(position)).setText(String.valueOf(value));
        }*/
    }

    private void fillAllEntriesInRandomGridView() {
//        initRandomHashMap();
        initAndShuffleArraylists();
        Integer position ;
        Integer value ;
        //sets whatever user has selected
        for (Map.Entry<Integer, Integer> entry : userHashMap.entrySet()) {
            //position
            position = entry.getKey();
            value = entry.getValue();
            //prepare the random hashmap also and remove position and value from respective arraylist
            positionArrayList.remove(position);
            valueArrayList.remove(value);
            randomNumberHashMap.put(position, value);

            ((TextView) randomNumberGridView.getChildAt(position)).setText(String.valueOf(value));
        }
        //rest is randomly filled
        fillRestEntriesInRandomGridView();

    }

    @Override
    public void onClick(View v) {

        if (R.id.shuffle_textview == v.getId())
        {
            shuffledClicked = true;
            shuffleRandomGridView();
        }
        else  if (R.id.play_textview == v.getId())
        {
            Intent intent = new Intent(BuildYourBingoActivity.this, BingoGameActivityWithScratch.class);
            if (userHashMap.size() == 25)
                 intent.putExtra("userBingo", userHashMap);
            else
                intent.putExtra("userBingo", randomNumberHashMap);
            startActivity(intent);
        }
    }

    private void shuffleRandomGridView() {
        initAndShuffleArraylists();
        fillRestEntriesInRandomGridView();
    }


}
