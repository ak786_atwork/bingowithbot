package com.example.bingowithbot.viewmodel;


import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;


public class MainActivityViewModel extends AndroidViewModel {

    private MutableLiveData<Integer> selectedValueMutableData;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        selectedValueMutableData = new MutableLiveData<>();
    }


    public MutableLiveData<Integer> getSelectedValueMutableData() {
        return selectedValueMutableData;
    }

    public void setSelectedValueMutableData(MutableLiveData<Integer> selectedValueMutableData) {
        this.selectedValueMutableData = selectedValueMutableData;
    }

}