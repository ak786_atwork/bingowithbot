package com.example.bingowithbot.helpers;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.bingowithbot.R;


public class CustomFontTextView extends TextView {

    String customFont;
    private boolean horizontalLine;
    private boolean verticalLine;
    private boolean diagonal1;
    private boolean diagonal2;

    public boolean isHorizontalLine() {
        return horizontalLine;
    }

    public void setHorizontalLine(boolean horizontalLine) {
        this.horizontalLine = horizontalLine;
    }

    public boolean isVerticalLine() {
        return verticalLine;
    }

    public void setVerticalLine(boolean verticalLine) {
        this.verticalLine = verticalLine;
    }

    public boolean isDiagonal1() {
        return diagonal1;
    }

    public void setDiagonal1(boolean diagonal1) {
        this.diagonal1 = diagonal1;
    }

    public boolean isDiagonal2() {
        return diagonal2;
    }

    public void setDiagonal2(boolean diagonal2) {
        this.diagonal2 = diagonal2;
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context, attrs);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context, attrs);

    }

    private void style(Context context, AttributeSet attrs) {

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.CustomFontTextView);
        int cf = a.getInteger(R.styleable.CustomFontTextView_fontName, 0);
        int fontName = 0;
        switch (cf) {
            case 1:
                fontName = R.string.Roboto_Bold;
                break;
            case 2:
                fontName = R.string.Roboto_Italic;
                break;
            case 3:
                fontName = R.string.Roboto_Light;
                break;
            case 4:
                fontName = R.string.Roboto_Medium;
                break;
            case 5:
                fontName = R.string.Roboto_Regular;
                break;
            case 6:
                fontName = R.string.Roboto_Thin;
                break;
            case 7:
                fontName = R.string.Bigtop;
                break;
            default:
                fontName = R.string.Roboto_Regular;
                break;
        }

        customFont = getResources().getString(fontName);

        Typeface tf = Typeface.createFromAsset(context.getAssets(),  "font/"+customFont + ".ttf");
        setTypeface(tf);
        a.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (horizontalLine)
            drawHorizontalLine(canvas);
        if (verticalLine)
            drawVerticalLine(canvas);
        if (diagonal1)
            drawFirstDiagonal(canvas);
        if (diagonal2)
            drawSecondDiagonal(canvas);

    }

    public void drawVerticalLine(Canvas canvas)
    {
        Rect rectf = new Rect();
        //For coordinates location relative to the parent
        getLocalVisibleRect(rectf);

        float startX = rectf.width()/2;
        float startY = rectf.top ;
        float endX = startX;
        float endY = startY + rectf.height();

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(15f);
        canvas.drawLine(startX,startY,endX,endY, paint);
    }

    public void drawHorizontalLine(Canvas canvas)
    {
        Rect rectf = new Rect();
        //For coordinates location relative to the parent
        getLocalVisibleRect(rectf);

        float startX = rectf.left;
        float startY = rectf.height()/2 ;
        float endX = startX + rectf.width();
        float endY = startY;

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(15f);
        canvas.drawLine(startX,startY,endX,endY, paint);
    }

    public void drawFirstDiagonal(Canvas canvas)
    {
        Rect rectf = new Rect();
        //For coordinates location relative to the parent
        getLocalVisibleRect(rectf);

        float startX = rectf.left;
        float startY = rectf.top;
        float endX = startX + rectf.width();
        float endY = startY + rectf.height();

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(15f);
        canvas.drawLine(startX,startY,endX,endY, paint);
    }

    public void drawSecondDiagonal(Canvas canvas)
    {
        Rect rectf = new Rect();
        //For coordinates location relative to the parent
        getLocalVisibleRect(rectf);

        float startX = rectf.width();
        float startY = rectf.top ;
        float endX = rectf.left;
        float endY = startY + rectf.height();

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(15f);
        canvas.drawLine(startX,startY,endX,endY, paint);
    }

    public void reDraw()
    {
        invalidate();
    }

}