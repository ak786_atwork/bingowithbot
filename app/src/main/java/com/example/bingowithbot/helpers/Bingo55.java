package com.example.bingowithbot.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Bingo55 {

    private int linesCompleted;
    private HashMap<Integer, Integer> positionHashMap;
    private HashMap<Integer, BingoHelper> valueHashMap;
    
    private  int[] allRowsNumberFilledCount;
    private  int[] allColumnsNumberFilledCount;
    private int[] allDiagonalsNumberFilledCount;

    private static final String BOT = "BOT";
    private static final String USER = "USER";
    private String userType = USER;
    private boolean checkLines;
    //this is for finding the which lines has been completed by a crossing a single value
    // size = 4 rowNumber, colNumber, diagonal1 , diagonal2 which will be prepared everytime
    private int[] linesToScratch;


    public Bingo55()
    {
        this.positionHashMap = prepareRandomBingoSheet();
        this.valueHashMap = getHashMapOfValueAsKey(positionHashMap);
        allColumnsNumberFilledCount = new int[Constants.COLUMN_SIZE];
        allRowsNumberFilledCount = new int[Constants.ROW_SIZE];
        allDiagonalsNumberFilledCount = new int[2];

        linesToScratch = new int[4];
    }


    public String getUserType() {
        return userType;
    }
    public void setUserType(String userType) {
        this.userType = userType;
    }

    public int getlinesCompleted() {
        return linesCompleted;
    }

    public void setlinesCompleted(int linesCompleted) {
        this.linesCompleted = linesCompleted;
    }

    public HashMap<Integer, Integer> getPositionHashMap() {
        return positionHashMap;
    }

    public void setPositionHashMap(HashMap<Integer, Integer> positionHashMap) {
        this.positionHashMap = positionHashMap;
        this.valueHashMap = getHashMapOfValueAsKey(positionHashMap);
    }

    public boolean isNumberHasAlreadySelected(int number)
    {
        return getValueHashMap().get(number).isSelected();
    }

    public HashMap<Integer, BingoHelper> getValueHashMap() {
        return valueHashMap;
    }

    public void setValueHashMap(HashMap<Integer, BingoHelper> valueHashMap) {
        this.valueHashMap = valueHashMap;
    }

    public HashMap<Integer,Integer> prepareRandomBingoSheet()
    {
        HashMap<Integer,Integer> positionHashMap = new HashMap<>();
        List<Integer> list = getNumberList();
        Random random = new Random();
        for (int i = 0; i < 25; i++)
        {
            positionHashMap.put(i, list.remove(random.nextInt(list.size())));
        }
        return positionHashMap;
    }

    private List<Integer> getNumberList()
    {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i < 26; i++)
            list.add(i);
        return list;
    }
    private HashMap<Integer, BingoHelper> getHashMapOfValueAsKey(HashMap<Integer, Integer> hashMapPosition)
    {
        HashMap<Integer, BingoHelper> hashMapValues = new HashMap<>();
        for (Map.Entry<Integer, Integer> entry : hashMapPosition.entrySet())
        {
            hashMapValues.put(entry.getValue(), new BingoHelper(entry.getKey()));
        }
        return hashMapValues;
    }

    public boolean haveYouWon() {
        return linesCompleted == 5; 
    }

    public void setValueAsCrossed(int value) {
        if (!valueHashMap.get(value).isSelected())
        {

            int rowColumn[] = Util.getRowColumnNumberOfPosition(valueHashMap.get(value).getPosition());
            allRowsNumberFilledCount[rowColumn[0]]++;
            allColumnsNumberFilledCount[rowColumn[1]]++;
            updateDiagonalsCount(rowColumn[0], rowColumn[1]);
            
            valueHashMap.get(value).setSelected(true);
            
            updateLinesCompleted(rowColumn[0],rowColumn[1]);
        }
           
    }

    public int[] getLinesToScratch()
    {
        return linesToScratch;
    }
    private void updateLinesCompleted(int row, int column) {
        linesToScratch[0] = -1;
        linesToScratch[1] = -1;
        if (allRowsNumberFilledCount[ row] == 5)
        {
            allRowsNumberFilledCount[row] = Integer.MIN_VALUE;
            linesCompleted++;
            linesToScratch[0] = row;
        }
        if ( allColumnsNumberFilledCount[column] == 5)
        {
            allColumnsNumberFilledCount[column] = Integer.MIN_VALUE;
            linesCompleted++;
            linesToScratch[1] = column;
        }
    }

    private void updateDiagonalsCount(int row, int column)
    {
        linesToScratch[2] = -1;
        linesToScratch[3] = -1;
        //check first diagonal 00 11 22 33 44
        if (row == column)
        {
            allDiagonalsNumberFilledCount[0]++;
            if (allDiagonalsNumberFilledCount[0] == 5)
            {
                allDiagonalsNumberFilledCount[0] = Integer.MIN_VALUE;
                linesCompleted++;
                //updating linesToScratch
                linesToScratch[2] = 1;
            }
        }
        //check second diagonal 04 13 22 31 40
        if (row + column == Constants.ROW_SIZE - 1)
        {
            allDiagonalsNumberFilledCount[1]++;
            if (allDiagonalsNumberFilledCount[1] == 5)
            {
                allDiagonalsNumberFilledCount[1] = Integer.MIN_VALUE;
                linesCompleted++;
                linesToScratch[3] = 1;
            }
        }
    }

    public int getNextMovePosition()
    {
        int currentWeight = 0, maxWeight = 0, position = -1;
        
        for(int i=0; i < 25; i++)
        {
            if (!valueHashMap.get(positionHashMap.get(i)).isSelected())
            {
                currentWeight = getWeight(i);
                if (currentWeight > maxWeight)
                {
                    maxWeight = currentWeight;
                    position = i;
                }
            }
        }
        
        return position;
    }

    private int getWeight(int position)
    {
        //check lines is true and you find any row going to be completed then increase weight by 5  i.e 4+1
        int rowColumnWeight[] = Util.getRowColumnNumberOfPosition(position);
        int weight = 0;

        if (getIndexOfFirstDiagonalWeight(rowColumnWeight[0],rowColumnWeight[1]) != -1)
        {
            weight++;
            if (allDiagonalsNumberFilledCount[0] + 1 == 5 &&  checkLines)
                weight += allDiagonalsNumberFilledCount[0] + 4;
        }
        if (getIndexOfSecondDiagonalWeight(rowColumnWeight[0],rowColumnWeight[1]) != -1)
        {
            weight++;
            if (allDiagonalsNumberFilledCount[1] + 1 == 5 && checkLines)
                weight += allDiagonalsNumberFilledCount[1] + 4;
        }
        if (allColumnsNumberFilledCount[rowColumnWeight[1]] + 1 == 5 && checkLines)
            weight += allColumnsNumberFilledCount[rowColumnWeight[1]] + 4;
        else
            weight += allColumnsNumberFilledCount[rowColumnWeight[1]];

        if (allRowsNumberFilledCount[rowColumnWeight[0]] + 1 ==5 && checkLines)
            weight += allRowsNumberFilledCount[rowColumnWeight[0]] + 4;
        else
            weight += allRowsNumberFilledCount[rowColumnWeight[0]];

        weight  += 2;
        
        // by placing this number we get the weight,  so we increase row count ,col  count and diagonal count by 1 if it lies
        return weight;
    }

    private int getIndexOfFirstDiagonalWeight(int row, int column)
    {
        //check first diagonal 00 11 22 33 44
        if (row == column)
            return 0;
        return -1;
    }
    private int getIndexOfSecondDiagonalWeight(int row, int column)
    {
        //check second diagonal 04 13 22 31 40
        if (row + column == Constants.ROW_SIZE - 1)
            return 1;
        return -1;
    }
    private int getIndexOfDiagonalWeight(int row, int column)
    {
        //check first diagonal 00 11 22 33 44
        if (row == column)
            return 0;
        //check second diagonal 04 13 22 31 40
        if (row + column == Constants.ROW_SIZE - 1)
            return 1;
        return -1;
    }

    public void setCheckLines(boolean b) {
        this.checkLines = b;
    }

    public boolean isCheckLines() {
        return checkLines;
    }

    public String getFormattedBotSheet()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" BOT \n");
        for (int i = 0; i < 5 ; i++)
        {
            for (int j = 0; j < 5 ; j++)
            {
                stringBuilder.append(positionHashMap.get( i * 5 + j) + " ");
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
