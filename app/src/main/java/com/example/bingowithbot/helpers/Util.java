package com.example.bingowithbot.helpers;

import android.view.View;
import android.view.animation.ScaleAnimation;

public class Util {

    public static  int[] getRowColumnNumberOfPosition(int position)
    {
        int rowColumn[] = new int[2];
        //row number
        rowColumn[0] = position/Constants.COLUMN_SIZE;
        rowColumn[1] = position % Constants.COLUMN_SIZE; //== 0 ? Constants.COLUMN_SIZE - 1 : (position % Constants.COLUMN_SIZE)-1;

        return rowColumn;
    }
    public static void scaleView(View view)
    {
        view.clearAnimation();
        ScaleAnimation scaleAnimation = new ScaleAnimation(1,1.5f, 1, 1.5f);
        scaleAnimation.setDuration(1000);
        view.setAnimation(scaleAnimation);
        scaleAnimation.startNow();
    }
}
