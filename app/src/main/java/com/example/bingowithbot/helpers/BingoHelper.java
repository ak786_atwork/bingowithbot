package com.example.bingowithbot.helpers;

public class BingoHelper {

    private int position;
    private boolean isSelected;


    public BingoHelper(int position) {
        this.position = position;
    }



    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
