package com.example.bingowithbot.helpers;

public class TurnDataHelper {

    public static byte[] prepareSyncMessage() {
        byte data[] = new byte[3];
        data[0] = 's';
        return data;
    }

    public static byte[] prepareTurnMessage(byte turn, byte value) {
        byte data[] = new byte[3];
        data[0] = 'r';
        data[1] = turn;
        data[2] = value;
        return data;
    }

    public static byte[] prepareFinishMessage()
    {
        byte data[] = new byte[3];
        data[0] = 'f';
        return data;
    }
}
